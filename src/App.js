import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'

function App(props) {
    const [state, setState] = useState("")
    const [api, setApi] = useState("lion")

    const handleInput = (event) => {
        let value = event.target.value;
        setState(() => value)
    }

    const handleButton = (event) => {
        setApi(state)
    }

    useEffect(() => props.FetApi(api), [api])
    return (
        <>
        
        <ul>

                {props.list.map((n, i) => {
                    return (
                        <li key={i}>
                            {n} 
                        </li>
                    )
                })}

        </ul>
            <input onChange={(event) => handleInput(event)}></input>
            <button onClick={(event) => handleButton(event)}>handleButton</button>
        </>
    )
}

const MapDispatchToProps = (globalstate) => {
    console.log(globalstate.rdc.list);
    return {
        list: globalstate.rdc.list
    }
}

const MapStateToProps = (dispatch) => {
    return {
        FetApi: (payload) => {
            dispatch({
                type: "FETCH_API",
                payload: payload,
            })
        }
    }
}
export default connect(MapDispatchToProps, MapStateToProps)(App)