import { combineReducers, createStore, applyMiddleware } from "redux";
import rdc from "./rdc";
import reduxSaga from "redux-saga";
import Saga from "../redux_saga/saga";

const saga = reduxSaga()

const globalstateredux = combineReducers({
    rdc
})

const store = createStore(globalstateredux, applyMiddleware(saga))
console.log(store)
saga.run(Saga)
export default store