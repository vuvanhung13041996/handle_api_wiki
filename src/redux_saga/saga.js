import { put, takeLatest, call } from "redux-saga/effects"

async function FetApi(key) {
    let datajson = await fetch(`https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&limit=30&format=json&search=${key}`)
    let data = await datajson.json()
    console.log(data);
    return data
}
function* handlFetApi({ type, payload }) {
    let data = yield call(() => FetApi(payload))
    yield put({
        type: "PUSHLIST",
        payload: data[1]
    })
}
function* Saga() {
    yield takeLatest("FETCH_API", (ojb) => handlFetApi(ojb))
}

export default Saga